/**
 * Exporting callback hell function for getting cards Info fro ListId
 */
// module.exports = function (cards, listID, callback){
//     setTimeout(()=>{
//         if(cards.hasOwnProperty(listID)){
//             return callback({ [ listID ] : [ ...cards[listID] ] } );
//         }else{
//             return callback();
//         }
//     }, 2 * 1000);
// }

/**
 * Exporting Promise function for getting cards Info fro ListId
 */
module.exports = function (cards, listID){
    return new Promise((resolve, reject)=>{
        setTimeout(()=>{
            if(cards.hasOwnProperty(listID)){
                resolve({ [ listID ] : [ ...cards[listID] ] } );
            }else{
                reject("List Id not found!");
            }
        }, 2 * 1000);
    });
}