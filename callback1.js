/**
 * Exporting callback hell function for getting board Info
 */
// module.exports = function (boardsData, boardID, callback){
//     setTimeout(()=>{
//         if(boardsData.id === boardID){
//             return callback( { ...boardsData } );
//         }else{
//             return callback();
//         }
//     }, 2 * 1000);
// }

/**
 * Exporting Promise function for getting board Info
 */
module.exports = function (boardsData, boardID){
    return new Promise((resolve, reject)=>{
        setTimeout(()=>{
            if(boardsData.id === boardID){
                resolve( { ...boardsData } );
            }else{
                reject("Board ID not Found");
            }
        }, 2 * 1000);
    });
}