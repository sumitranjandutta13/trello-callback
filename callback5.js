const selectedBoard = require('./callback1');
const selectedList = require('./callback2');
const selectedCards = require('./callback3');

/**
 * Exporting callback hell function for getting thanos-Info, stones-List and mind-SpaceStone-Card
 */
// module.exports = function (boards, lists, cards){
//     setTimeout(()=>{
//         selectedBoard(boards, "def453ed", (thanosInfo) =>{
//             console.log(thanosInfo);
//             selectedList(lists, thanosInfo.id, (thanosStonesLists) =>{
//                 console.log(thanosStonesLists);
//                 let mindSpaceStoneInfo = thanosStonesLists[thanosInfo.id]
//                                     .filter(stoneInfo => {
//                                         return stoneInfo.name === 'Mind' || stoneInfo.name === 'Space';
//                                     })
//                 mindSpaceStoneInfo.forEach(stoneInfo =>{
//                     selectedCards(cards, stoneInfo.id, (stoneCards)=>{
//                         console.log(stoneCards);
//                     })
//                 })
//             })
//         })
//     }, 2000);
// }

/**
 * Exporting Promises function for getting board Info
 */
module.exports = async function (boards, lists, cards){

    try{
        const thanosBoardInfo = await selectedBoard(boards, "def453ed");
        const thanosStonesList = await selectedList(lists, thanosBoardInfo.id);
        let mindSpaceStoneInfo = thanosStonesList[thanosBoardInfo.id]
            .filter( stone =>{
                return stone.name === "Mind" || stone.name === "Space";
            })
        const thanosStonesCard = await Promise.all(mindSpaceStoneInfo.map((stone) =>{
            return selectedCards(cards, stone.id);
        }));
        console.log(thanosBoardInfo);
        console.log(thanosStonesList);
        thanosStonesCard.forEach(stoneInfo =>{
            console.log(stoneInfo)
        });
    }catch(err){
        console.log(err);
    }
}