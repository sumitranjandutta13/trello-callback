/**
 * Exporting callback hell function for getting List Info for BoardId
 */
// module.exports = function (lists, boardID, callback){
//     setTimeout(()=>{
//         if(lists.hasOwnProperty(boardID)){
//             return callback( { [boardID] : [ ...lists[boardID] ] } );
//         }else{
//             return callback();
//         }
//     }, 2 * 1000);
// }

/**
 * Exporting Promise function for getting List Info for BoardId
 */
module.exports = function (lists, boardID){
    return new Promise((resolve, reject)=>{
        setTimeout(()=>{
            if(lists.hasOwnProperty(boardID)){
                resolve( { [boardID] : [ ...lists[boardID] ] } );
            }else{
                reject("Board Id not Found in List!");
            }
        }, 2 * 1000); 
    });
}