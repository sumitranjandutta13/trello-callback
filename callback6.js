const selectedBoard = require('./callback1');
const selectedList = require('./callback2');
const selectedCards = require('./callback3');

/**
 * Exporting callback hell function for getting thanos-Info, stones-List and allStone-Card
 */
// module.exports = function (boards, lists, cards){
//     setTimeout(()=>{
//         selectedBoard(boards, "def453ed", (thanosInfo) =>{
//             console.log(thanosInfo);
//             selectedList(lists, thanosInfo.id, (thanosStonesLists) =>{
//                 console.log(thanosStonesLists);
//                 thanosStonesLists[thanosInfo.id].forEach(stoneInfo =>{
//                     selectedCards(cards, stoneInfo.id, (stoneCards)=>{
//                         if(stoneCards === undefined){
//                             console.log(`No cards found for ${stoneInfo.name} stone found!`)
//                         }else{
//                             console.log(stoneCards);
//                         }
//                     });
//                 })
//             })
//         })
//     }, 2000);
// }

/**
 * Exporting Promises function for getting thanos-Info, stones-List and allStone-Card
 */
module.exports = async function (boards, lists, cards){

    try{
        const thanosBoardInfo = await selectedBoard(boards, "def453ed");
        const thanosStonesList = await selectedList(lists, thanosBoardInfo.id);
        const thanosStonesCard = await Promise.allSettled(thanosStonesList[thanosBoardInfo.id]
            .map((stone) =>{
                return selectedCards(cards, stone.id);
            })
        );
        console.log(thanosBoardInfo);
        console.log(thanosStonesList);
        thanosStonesCard.forEach(stoneInfo =>{
            if(stoneInfo.status === 'fulfilled'){
                console.log(stoneInfo.value);
            }else{
                console.log(stoneInfo.reason);
            }
        });
    }catch(err){
        console.log(err);
    }
}