const selectedBoard = require('./callback1');
const selectedList = require('./callback2');
const selectedCards = require('./callback3');

/**
 * Exporting callback hell function for getting thanos-Info, stones-List and mindStone-Card
 */
module.exports = function (boards, lists, cards){

    setTimeout(()=>{
        selectedBoard(boards, "def453ed", (thanosInfo) =>{
            console.log(thanosInfo);
            selectedList(lists, thanosInfo.id, (thanosStonesLists) =>{
                console.log(thanosStonesLists);
                let mindStoneInfo = thanosStonesLists[thanosInfo.id]
                                    .filter(stoneInfo => {
                                        return stoneInfo.name === 'Mind'
                                    })
                selectedCards(cards, mindStoneInfo[0].id, (mindCards)=>{
                    console.log(mindCards);
                })
            })
        })
    }, 2000);
}

/**
 * Exporting Promises function for getting thanos-Info, stones-List and mindStone-Card
 */
module.exports = async function (boards, lists, cards){

    try{
        const thanosBoardInfo = await selectedBoard(boards, "def453ed");
        const thanosStonesList = await selectedList(lists, thanosBoardInfo.id);
        let mindStoneInfo = thanosStonesList[thanosBoardInfo.id]
            .filter( stone =>{
                return stone.name === "Mind";
            })
        const thanosStonesCard = await selectedCards(cards, mindStoneInfo[0].id);
        console.log(thanosBoardInfo);
        console.log(thanosStonesList);
        console.log(thanosStonesCard);
    }catch(err){
        console.log(err);
    }   
}