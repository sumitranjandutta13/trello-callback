const selectedBoard = require('../callback1');

const boards = require('../data/boards.json');

/**
 * Calling callback hell function imported from callback1.js
 */
// selectedBoard(boards, "def453ed", (data)=>{
//     if(data === undefined){
//         console.log("Selected Board Information not Found!!!");
//     }else{
//         console.log("Selected Board Information Acquired!!!");
//         console.log(data);
//     }
// });


/**
 * Calling Promise function imported from callback1.js
 */
selectedBoard(boards, "def453ed")
    .then(data =>{
        console.log(data);
    })
    .catch(err =>{
        console.log(err);
    });