const selectedCards = require('../callback3');

const cards = require('../data/cards.json');

/**
 * Calling callback hell function imported from callback3.js
 */
// selectedCards(cards, "jwkh245", (data)=>{
//     if(data === undefined){
//         console.log("Selected Card Information not Found!!!");
//     }else{
//         console.log("Selected Card Information Acquired!!!");
//         console.log(data);
//     }
// });

/**
 * Calling Promises function imported from callback3.js
 */
selectedCards(cards, "jwkh245")
    .then(data =>{
        console.log(data);
    })
    .catch(err =>{
        console.log(err);
    });