const getThanosMindCards = require('../callback4');

const boards = require('../data/boards.json');
const lists = require('../data/lists.json');
const cards = require('../data/cards.json');

/**
 * Calling function imported from callback4.js
 */
getThanosMindCards(boards, lists, cards);