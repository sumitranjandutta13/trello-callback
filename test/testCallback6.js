const getThanosAllCards = require('../callback6');

const boards = require('../data/boards.json');
const lists = require('../data/lists.json');
const cards = require('../data/cards.json');

/**
 * Calling function imported from callback4.js
 */
getThanosAllCards(boards, lists, cards);