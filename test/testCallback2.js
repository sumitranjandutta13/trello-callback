const selectedList = require('../callback2');

const lists = require('../data/lists.json');

/**
 * Calling callback hell function imported from callback2.js
 */
// selectedList(lists, "def453ed", (data)=>{
//     if(data === undefined){
//         console.log("Selected List Information not Found!!!");
//     }else{
//         console.log("Selected List Information Acquired!!!");
//         console.log(data);
//     }
// });

/**
 * Calling Promise Function imported from callback2.js
 */
selectedList(lists, "def453ed")
    .then(data =>{
        console.log(data);
    })
    .catch(err =>{
        console.log(err);
    });